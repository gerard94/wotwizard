#!/bin/bash

version="$1"

if [ -z $version ]; then
    echo "Error: Version argument is missing."
    exit 1
fi

docker buildx build --platform linux/amd64 -t poka/wotwizard:"$version" -t poka/wotwizard:latest . --push

