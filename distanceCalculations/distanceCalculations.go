/* 
WotWizard

Copyright (C) 2017-2020 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package distanceCalculations

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	BA	"git.duniter.org/gerard94/wotwizard/basic"
	G	"git.duniter.org/gerard94/util/graphQL"
	GQ	"git.duniter.org/gerard94/wotwizard/gqlReceiver"
	IS	"git.duniter.org/gerard94/wotwizard/identitySearchList"
	M	"git.duniter.org/gerard94/util/misc"
	S	"git.duniter.org/gerard94/util/sets3"
	SB	"git.duniter.org/gerard94/wotwizard/sandbox"

)

type (
	
	// Used in 'bestDist' and 'bestQual' to store and sort the best answers; stores one identity ('id'), her uid ('uid'), and her distance or quality ('num', 'denom' and 'ratio')
	idVal struct {
		uid string
		id SB.Hash
		num,
		denom int
		ratio float64
	}
	
	// Used in 'calcDQ'
	calcDistQualT func (pub B.Pubkey, member bool, listOfPubkeys B.PubkeysT) (num, denom int, ratio float64)
	
	calcDistQualF func (uid string, uidsOfNewcertifiers []string) (num, denom int, ratio float64)

)

// Gets the pubkey, the hash, the membership, the presence in blockchain, of the identity whose uid is 'uid'; if !'ok', the identity is unknown
func get (uid string) (pub B.Pubkey, hash SB.Hash, member, inBC, ok bool) {
	pub, member, hash, _, _, _, ok = B.IdUidComplete(uid)
	inBC = ok
	if !ok {
		pos := SB.IdPosUid(uid)
		id, h, b := SB.IdNextUid(false, &pos)
		ok = b && id == uid
		if ok {
			member = false
			hash = h
		}
	}
	return
} //get

// Used in 'calcDQ'; adds the pubkeys of the identities whose uids are in 'uids' to 'ps' an returns the result
func uidsToPubs (ps B.PubkeysT, uids []string) B.PubkeysT {
	for _, id := range uids {
		if pub, ok := B.IdUid(id); ok {
			ps = append(ps, pub)
		}
	}
	return ps
} //uidsToPubs

// Used in 'calcDist'; calculates the expected distance of 'p', who is member or not ('member'), when she is certified by the extra certifiers 'certs'
func notTooFar (p B.Pubkey, member bool, certs B.PubkeysT) (num, denom int, ratio float64) {
	num, denom, ratio, _ = IS.NotTooFar(p, member, certs)
	return
} //notTooFar

// Common part of 'calcDist' and 'calcQual'
func calcDQ (uid string, certifiers []string, f calcDistQualT) (num, denom int, ratio float64) {
	var certs = make(B.PubkeysT, 0)
	p, h, member, inBC, ok := get(uid)
	if ok {
		_, certs = IS.RecCerts(h, p, inBC, true)
	}
	return f(p, member, uidsToPubs(certs, certifiers))
} //calcDQ

// Implements the graphQL field 'Query.calcDist'
func calcDist (uid string, certifiers []string) (num, denom int, ratio float64) {
	return calcDQ(uid, certifiers, notTooFar)
} //calcDist

// Implements the graphQL field 'Query.calcQual'
func calcQual (uid string, certifiers []string) (num, denom int, ratio float64) {
	return calcDQ(uid, certifiers, IS.CalcQuality)
} //calcQual

// Comparison method of '*idVal' in '*ATree' : greatest 'ratio's first
func (iv1 *idVal) Compare (iv2 A.Comparer) A.Comp {
	i2, ok := iv2.(*idVal); M.Assert(ok, 100)
	if iv1.ratio > i2.ratio {
		return A.Lt
	}
	if iv1.ratio < i2.ratio {
		return A.Gt
	}
	return BA.CompP(iv1.uid, i2.uid)
} //Compare

// *S.Set -> []string; used in 'bestDQ', 'calcDistR' and 'calcQualR'
func set2Slice (set *S.Set) []string {
	ls := make([]string, set.NbElems())
	it := set.Attach()
	i := 0
	s, ok := it.FirstE()
	for ok {
		ls[i] = s
		i++
		s, ok = it.NextE()
	}
	return ls
} //set2Slice

// 't' contains the '*idVal' structs with the greatest values of 'idVal.ratio' found so far; 'insert' inserts 'iv' into 't' if its 'ratio' is great enough, while maintaining the size of 't' less or equal to 'max', unless 'max' <= 0
func insert (t *A.Tree, iv *idVal, max int, n *int, greatest **idVal) {
	if max <= 0 || *n < max || iv.Compare(*greatest) == A.Lt {
		_, b, _ := t.SearchIns(iv)
		M.Assert(!b, 100)
		if max > 0 {
			if *n < max {
				*n++
			} else {
				t.Erase(max + 1)
			}
			if *n == max {
				e, found := t.Find(max); M.Assert(found, 101)
				*greatest =e.Val().(*idVal)
			}
		}
	}
} //insert

// Verifies that the identity whose uid is 'uid' has not drained yet her certification stock
func hasCertsEnough (uid string, maxCerts int) bool {
	pub, b := B.IdUid(uid); M.Assert(b, 100)
	pos := new(B.CertPos)
	B.CertFrom(pub, pos)
	return pos.CertPosLen() < maxCerts
} //hasCertsEnough

// Common part of 'bestDist' and 'besQual'
func bestDQ (group *S.Set, groupIsEmpty bool, uid string, certifiers *S.Set, answerNb int, f calcDistQualF) *A.Tree { //*idVal
	p, h, member, inBC, ok := get(uid)
	var cs = make(B.PubkeysT, 0)
	if ok {
		_, cs = IS.RecCerts(h, p, inBC, true)
	}
	allCerts := S.NewSet()
	if member {
		allCerts.Incl(uid)
	}
	for _, c := range cs {
		if uid, ok := B.IdPub(c); ok {
			allCerts.Incl(uid)
		}
	}
	allCerts.Add(certifiers)
	
	maxCerts := int(B.Pars().SigStock)
	certs := set2Slice(certifiers)
	t := A.New()
	var (n = 0; greatest *idVal)
	if groupIsEmpty {
		var pos *B.Position
		uid2, ok := B.IdNextUidM(true, &pos)
		for ok {
			if !allCerts.In(uid2) && hasCertsEnough(uid2, maxCerts) {
				_, _, hash, _, _, _ , b := B.IdUidComplete(uid2); M.Assert(b, 100)
				newCerts := append(certs, uid2)
				num, denom, ratio := f(uid, newCerts)
				insert(t, &idVal{uid: uid2, id:hash, num: num, denom: denom, ratio: ratio}, answerNb, &n, &greatest)
			}
			uid2, ok = B.IdNextUidM(false, &pos)
		}
	} else {
		it := group.Attach()
		uid2, ok := it.FirstE()
		for ok {
			if _, member, _, _, _, _, ok := B.IdUidComplete(uid2); ok && member && !allCerts.In(uid2) && hasCertsEnough(uid2, maxCerts) {
				_, _, hash, _, _, _ , b := B.IdUidComplete(uid2)
				M.Assert(b, 101)
				newCerts := append(certs, uid2)
				num, denom, ratio := f(uid, newCerts)
				insert(t, &idVal{uid: uid2, id:hash, num: num, denom: denom, ratio: ratio}, answerNb, &n, &greatest)
			}
			uid2, ok = it.NextE()
		}
	}
	return t
} //bestDQ

// Implements the graphQL field 'Query.bestDist'
func bestDist (group *S.Set, groupIsEmpty bool, uid string, certifiers *S.Set, answerNb int) *A.Tree {
	return bestDQ(group, groupIsEmpty, uid, certifiers, answerNb, calcDist)
} //bestDist

// Implements the graphQL field 'Query.bestQual'
func bestQual (group *S.Set, groupIsEmpty bool, uid string, certifiers *S.Set, answerNb int) *A.Tree {
	return bestDQ(group, groupIsEmpty, uid, certifiers, answerNb, calcQual)
} //bestQual

// Gets argument 'uid' as a '*G.StringValue'; raises an error if 'uid' doesn't correspond to a known identity
func getUid (ts G.TypeSystem, argumentValues *A.Tree) string {
	
	makePath := func (name string) G.Path {
		p := G.NewPathBuilder()
		p = p.PushPathString(name)
		return p.GetPath()
	}

	var v G.Value
	ok := G.GetValue(argumentValues, "uid", &v)
	if !ok {
		return ""
	}
	switch v := v.(type) {
	case *G.StringValue:
		s := v.String.S
		if s != "" {
			if _, ok := B.IdUid(s); !ok {
				ts.Error(s + " is unknown", nil, makePath("uid"))
				return ""
			}
		}
		return s
	case *G.NullValue:
		return ""
	default:
		M.Halt(v, 101)
		return ""
	}
} //getUid

// Gets argument 'uid' as int
func getAnswerNb (argumentValues *A.Tree) int {
	var v G.Value
	ok := G.GetValue(argumentValues, "answerNb", &v)
	if !ok {
		return 0
	}
	switch v := v.(type) {
	case *G.IntValue:
		return int(v.Int)
	case *G.NullValue:
		return 0
	default:
		M.Halt(v, 101)
		return 0
	}
} //getAnswerNb

// Gets the argument of name 'name', which is a list of strings, as a '*S.Set'; 'isEmpty' returns whether the list is empty; if 'onlyMember', only members are returned in the list
func getStringList (ts G.TypeSystem, argumentValues *A.Tree, name string, onlyMember bool, isEmpty *bool) *S.Set {
	
	makePath := func (name string, n int) G.Path {
		p := G.NewPathBuilder()
		p = p.PushPathString(name)
		p = p.PushPathNb(n)
		return p.GetPath()
	}
	
	var v G.Value
	ok := G.GetValue(argumentValues, name, &v)
	if !ok {
		*isEmpty = true
		return S.NewSet()
	}
	switch v := v.(type) {
	case *G.ListValue:
		l := S.NewSet()
		i := 0
		e := v.First()
		if e == nil {
			*isEmpty = true
			return l
		}
		*isEmpty = false
		for e != nil {
			switch vv := e.Value.(type) {
			case *G.StringValue:
				s := vv.String.S
				if s != "" {
					var ok bool
					if onlyMember {
						_, ok = B.IdUidM(s)
					} else {
						_, ok = B.IdUid(s)
					}
					if ok {
						if !l.Incl(s) && GQ.GroupDisplayDuplicateError {
							ts.Error(s + " is duplicated", nil, makePath(name, i))
						}
					} else if onlyMember {
						if GQ.GroupDisplayNotMemberError {
							ts.Error(s + " isn't a member", nil, makePath(name, i))
						}
					} else if GQ.GroupDisplayUnknownError {
						ts.Error(s + " is unknown", nil, makePath(name, i))
					}
				}
			default:
				M.Halt(e.Value, 101)
				return nil
			}
			i++
			e = v.Next(e)
		}
		return l
	case *G.NullValue:
		*isEmpty = true
		return S.NewSet()
	default:
		M.Halt(v, 102)
		return nil
	}
} //getStringList

func calcDistR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var b bool
	num, denom, ratio := calcDist(getUid(ts, argumentValues), set2Slice(getStringList(ts, argumentValues, "certifiers", true, &b)))
	return G.Wrap(G.Wrap(num, denom, ratio), ratio >= B.Pars().Xpercent)
} //calcDistR

func calcQualR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var b bool
	num, denom, ratio := calcQual(getUid(ts, argumentValues), set2Slice(getStringList(ts, argumentValues, "certifiers", true, &b)))
	return G.Wrap(num, denom, ratio)
} //calcQualR

// *A.Tree -> 'IdDist' for 'bestDistR' as (id, ((num, denom, ratio), dist_ok)), or 'IdQual' for 'bestQualR' as (id, (num, denum ratio))
func bestDQCommon (t *A.Tree, dist bool) G.Value {
	l := G.NewListValue()
	e := t.Next(nil)
	for e != nil {
		iv := e.Val().(*idVal)
		fd := G.Wrap(iv.num, iv.denom, iv.ratio)
		if dist {
			fd = G.Wrap(fd, iv.ratio >= B.Pars().Xpercent)
		}
		l.Append(G.Wrap(iv.id, fd))
		e = t.Next(e)
	}
	return l
} //bestDQCommon

func bestDistR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var groupIsEmpty, b bool
	group := getStringList(ts, argumentValues, "group", false, &groupIsEmpty)
	certifiers := getStringList(ts, argumentValues, "certifiers", true, &b)
	t := bestDist(group, groupIsEmpty, getUid(ts, argumentValues), certifiers, getAnswerNb(argumentValues))
	return bestDQCommon(t, true)
} //bestDistR

func bestQualR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var groupIsEmpty, b bool
	group := getStringList(ts, argumentValues, "group", false, &groupIsEmpty)
	certifiers := getStringList(ts, argumentValues, "certifiers", true, &b)
	t := bestQual(group, groupIsEmpty, getUid(ts, argumentValues), certifiers, getAnswerNb(argumentValues))
	return bestDQCommon(t, false)
} //bestQualR

func IdValIdR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	hash := G.Unwrap(rootValue, 0)
	switch hash := hash.(type) {
	case SB.Hash:
		return G.Wrap(hash)
	default:
		M.Halt(100, hash)
		return nil
	}
} //IdValIdR

func IdValValR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 1)
	val, ok := v.(*G.OutputObjectValue); M.Assert(ok, v, 100)
	return val
} //IdValValR

func fixFieldResolvers (ts G.TypeSystem) {
	ts.FixFieldResolver("Query", "calcDist", calcDistR)
	ts.FixFieldResolver("Query", "calcQual", calcQualR)
	ts.FixFieldResolver("Query", "bestDist", bestDistR)
	ts.FixFieldResolver("Query", "bestQual", bestQualR)
	ts.FixFieldResolver("IdDist", "id", IdValIdR)
	ts.FixFieldResolver("IdDist", "dist", IdValValR)
	ts.FixFieldResolver("IdQual", "id", IdValIdR)
	ts.FixFieldResolver("IdQual", "qual", IdValValR)
} //fixFieldResolvers

func init () {
	fixFieldResolvers(GQ.TS())
}
