/* 
WotWizard

Copyright (C) 2017-2020 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package history

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	G	"git.duniter.org/gerard94/util/graphQL"
	GQ	"git.duniter.org/gerard94/wotwizard/gqlReceiver"
	IS	"git.duniter.org/gerard94/wotwizard/identitySearchList"
	M	"git.duniter.org/gerard94/util/misc"

)

type (
	
	event struct {
		next *event
		block int32
	}

)

func buildHistory (pubkey B.Pubkey) *event {
	var ev *event = nil
	var jb, lb int32
	list, ok := B.JLPub(pubkey)
	if ok {
		jb, lb, ok = B.JLPubLNext(&list); M.Assert(ok, 100)
	}
	for ok {
		if lb != B.HasNotLeaved {
			ev = &event{next: ev}
			ev.block = lb
		}
		ev = &event{next: ev}
		ev.block = jb
		jb, lb, ok = B.JLPubLNext(&list)
	}
	return ev
} //buildHistory

func identityHistoryR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		l := G.NewListValue()
		_, pub, _, _, _, _, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if inBC {
			ev := buildHistory(pub)
			in := true
			for ev != nil {
				l.Append(G.Wrap(in, ev.block))
				in = !in
				ev = ev.next
			}
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityHistoryR

func historyEvInR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch in := G.Unwrap(rootValue, 0).(type) {
	case bool:
		return G.MakeBooleanValue(in)
	default:
		M.Halt(in, 100)
		return nil
	}
} //historyEvInR

func historyEvBlockR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch block := G.Unwrap(rootValue, 1).(type) {
	case int32:
		return G.Wrap(block)
	default:
		M.Halt(block, 100)
		return nil
	}
} //historyEvBlockR

func fixFieldResolvers (ts G.TypeSystem) {
	ts.FixFieldResolver("Identity", "history", identityHistoryR)
	ts.FixFieldResolver("HistoryEvent", "in", historyEvInR)
	ts.FixFieldResolver("HistoryEvent", "block", historyEvBlockR)
} //fixFieldResolvers

func init () {
	fixFieldResolvers(GQ.TS())
} //init
