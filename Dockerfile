FROM golang:1.22.0 AS builder

WORKDIR /app

COPY . .

# Prepare go mods
RUN go mod init git.duniter.org/gerard94/wotwizard
RUN go mod tidy

# Build wwServer
RUN go build -o builds/wwServer ./wwServer/wwServer.go

FROM debian:latest

WORKDIR /app

COPY --from=builder /app/builds/wwServer .
COPY scripts/start-docker.sh .

EXPOSE 8080

CMD ["./start-docker.sh"]
