/* 
WotWizard

Copyright (C) 2017-2020 Gérard Meunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package run

import (
	
	_	"git.duniter.org/gerard94/util/babel/static"
	_	"git.duniter.org/gerard94/util/json/static"
	_	"git.duniter.org/gerard94/util/graphQL/static"
	_	"git.duniter.org/gerard94/wotwizard/static"
	
	A	"git.duniter.org/gerard94/util/avl"
	BA	"git.duniter.org/gerard94/wotwizard/basic"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	GQ	"git.duniter.org/gerard94/wotwizard/gqlReceiver"
	G	"git.duniter.org/gerard94/util/graphQL"
	M	"git.duniter.org/gerard94/util/misc"
	S	"git.duniter.org/gerard94/wotwizard/sandbox"
	
	_	"git.duniter.org/gerard94/wotwizard/blocks"
	_	"git.duniter.org/gerard94/wotwizard/certifications"
	_	"git.duniter.org/gerard94/wotwizard/distanceCalculations"
	_	"git.duniter.org/gerard94/wotwizard/events"
	_	"git.duniter.org/gerard94/wotwizard/history"
	_	"git.duniter.org/gerard94/wotwizard/identities"
	_	"git.duniter.org/gerard94/wotwizard/members"
	_	"git.duniter.org/gerard94/wotwizard/parameters"
	_	"git.duniter.org/gerard94/wotwizard/sentries"
	_	"git.duniter.org/gerard94/wotwizard/wotWizardList"
	
		"fmt"

)

const (
	
	version = "5.11.3"

)

func Start () {
	fmt.Println("WotWizard Server version", version, "Tools version", M.Version(), "\n")
	BA.Lg.Println("WotWizard Server version", version, "Tools version", M.Version(), "\n")
	B.Initialize()
	S.Initialize()
	GQ.Start()
} //Start

func versionR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	return G.MakeStringValue(version)
} //versionR

func init () {
	ts := GQ.TS()
	ts.FixFieldResolver("Query", "version", versionR)
} //init
