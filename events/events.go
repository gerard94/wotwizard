/* 
WotWizard

Copyright (C) 2017-2020 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package events

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	BA	"git.duniter.org/gerard94/wotwizard/basic"
	G	"git.duniter.org/gerard94/util/graphQL"
	GQ	"git.duniter.org/gerard94/wotwizard/gqlReceiver"
	M	"git.duniter.org/gerard94/util/misc"
	S	"git.duniter.org/gerard94/util/sets3"

)

type (
	
	membership struct {
		uid string
		exp int64
	}

)

func (m1 *membership) Compare (m2 A.Comparer) A.Comp {
	mm2, ok := m2.(*membership); M.Assert(ok, 100)
	if M.Abs64(m1.exp) < M.Abs64(mm2.exp) {
		return A.Lt
	}
	if M.Abs64(m1.exp) > M.Abs64(mm2.exp) {
		return A.Gt
	}
	return BA.CompP(m1.uid, mm2.uid)
}

func filter (t *A.Tree, start, end int64) *A.Tree {
	_, _, beg := t.SearchNext(&membership{uid:"", exp:start})
	if beg == 0 { // after the end of the tree
		t.Empty()
		return t
	}
	t = t.Split(beg - 1)
	_, _, stop := t.SearchNext(&membership{uid:"", exp:end})
	if stop != 0 { // before or at the end of the tree
		t.Split(stop - 1)
	}
	return t
}

func doMembershipsEnds (group *S.Set, start, end int64, groupIsEmpty bool) *A.Tree {
	t := A.New()
	if groupIsEmpty {
		var pst *B.Position
		uid, ok := B.IdNextUidM(true, &pst)
		for ok {
			_, mem, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b && mem, 100)
			_, b, _ = t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 101)
			uid, ok = B.IdNextUidM(false, &pst)
		}
	} else {
		it := group.Attach()
		uid, ok := it.FirstE()
		for ok {
			_, mem, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 102)
			if mem {
				_, b, _ := t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 103)
			}
			uid, ok = it.NextE()
		}
	}
	return filter(t, start, end)
}

func doMissingEnds (group *S.Set, start, end int64, groupIsEmpty bool) *A.Tree {
	t := A.New()
	if groupIsEmpty {
		var pst *B.Position
		uid, ok := B.IdNextUid(true, &pst)
		for ok {
			_, mem, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 100)
			if !mem && exp != BA.Revoked {
				_, b, _ := t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 101)
			}
			uid, ok = B.IdNextUid(false, &pst)
		}
	} else {
		it := group.Attach()
		uid, ok := it.FirstE()
		for ok {
			_, mem, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 102)
			if !mem && exp != BA.Revoked {
				_, b, _ := t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 103)
			}
			uid, ok = it.NextE()
		}
	}
	return filter(t, start, end)
}

func doCertifsEnds (group *S.Set, start, end int64, missingIncluded, groupIsEmpty bool) *A.Tree {
	t := A.New()
	n := int(B.Pars().SigQty)
	if groupIsEmpty {
		var pst *B.Position
		uid, ok := B.IdNextUid(true, &pst)
		for ok {
			p, member, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 100)
			var pos B.CertPos
			if (missingIncluded && exp != BA.Revoked || !missingIncluded && member) && B.CertToByExp(p, &pos) && pos.CertPosLen() >= n {
				var from, to B.Pubkey
				for j := 1; j <= n; j++ {
					from, to, b = pos.CertNextPos(); M.Assert(b, 101)
				}
				_, exp, b = B.Cert(from, to); M.Assert(b, 102)
				_, b, _ := t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 103)
			}
			uid, ok = B.IdNextUid(false, &pst)
		}
	} else {
		it := group.Attach()
		uid, ok := it.FirstE()
		for ok {
			p, member, _, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 104)
			var pos B.CertPos
			if (missingIncluded && exp != BA.Revoked || !missingIncluded && member) && B.CertToByExp(p, &pos) && pos.CertPosLen() >= n {
				var from, to B.Pubkey
				for j := 1; j <= n; j++ {
					from, to, b = pos.CertNextPos(); M.Assert(b, 105)
				}
				_, exp, b = B.Cert(from, to); M.Assert(b, 106)
				_, b, _ := t.SearchIns(&membership{uid:uid, exp:exp}); M.Assert(!b, 103)
			}
			uid, ok = it.NextE()
		}
	}
	return filter(t, start, end)
}

var (
	
	memStream = GQ.CreateStream("memEnds")
	missStream = GQ.CreateStream("missEnds")
	certsStream = GQ.CreateStream("certEnds")

)

func memStreamResolver (rootValue *G.OutputObjectValue, argumentValues *A.Tree) *G.EventStream { // *G.ValMapItem
	return memStream
} //memStreamResolver

func missStreamResolver (rootValue *G.OutputObjectValue, argumentValues *A.Tree) *G.EventStream { // *G.ValMapItem
	return missStream
} //missStreamResolver

func certStreamResolver (rootValue *G.OutputObjectValue, argumentValues *A.Tree) *G.EventStream { // *G.ValMapItem
	return certsStream
} //certStreamResolver

func getStringList (ts G.TypeSystem, argumentValues *A.Tree, name string, isEmpty *bool) *S.Set {
	
	makePath := func (name string, n int) G.Path {
		p := G.NewPathBuilder()
		p = p.PushPathString("variables")
		p = p.PushPathString(name)
		p = p.PushPathNb(n)
		return p.GetPath()
	}
	
	var v G.Value
	ok := G.GetValue(argumentValues, name, &v)
	if !ok {
		*isEmpty = true
		return S.NewSet()
	}
	switch v := v.(type) {
	case *G.ListValue:
		l := S.NewSet()
		i := 0
		e := v.First()
		if e == nil {
			*isEmpty = true
			return l
		}
		*isEmpty = false
		for e != nil {
			switch vv := e.Value.(type) {
			case *G.StringValue:
				s := vv.String.S
				if s != "" {
					_, ok := B.IdUid(s)
					if ok {
						if !l.Incl(s) && GQ.GroupDisplayDuplicateError {
							ts.Error(s + " is duplicated", nil, makePath(name, i))
						}
					} else if GQ.GroupDisplayUnknownError {
						ts.Error(s + " is unknown", nil, makePath(name, i))
					}
				}
			default:
				M.Halt(e.Value, 101)
				return nil
			}
			i++
			e = v.Next(e)
		}
		return l
	case *G.NullValue:
		*isEmpty = true
		return S.NewSet()
	default:
		M.Halt(v, 102)
		return nil
	}
} //getStringList

func getStartEnd (as *A.Tree) (start, end int64) {// start included, end excluded
	var v G.Value
	if G.GetValue(as, "startFromNow", &v) {
		switch v := v.(type) {
		case *G.IntValue:
			start = v.Int
		case *G.NullValue:
			start = 0
		default:
			M.Halt(v, 100)
		}
	} else {
		start = 0
	}
	start += B.Now()
	if G.GetValue(as, "period", &v) {
		switch v := v.(type) {
		case *G.IntValue:
			end = v.Int
		case *G.NullValue:
			end = M.MaxInt64
		default:
			M.Halt(v, 100)
		}
	} else {
		end = M.MaxInt64
	}
	if end < M.MaxInt64 - start {
		end += start
	} else {
		end = M.MaxInt64
	}
	return
} //getStartEnd

func memEndsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var isEmpty bool
	group := getStringList(ts, argumentValues, "group", &isEmpty)
	start, end := getStartEnd(argumentValues)
	l := G.NewListValue()
	if start >= end {
		return l
	}
	t := doMembershipsEnds(group, start, end, isEmpty)
	e := t.Next(nil)
	for e != nil {
		m, ok := e.Val().(*membership); M.Assert(ok, 100)
		_, _, h, _, _, _, b := B.IdUidComplete(m.uid); M.Assert(b, 101)
		l.Append(G.Wrap(h))
		e = t.Next(e)
	}
	return l
} //memEndsR

func missEndsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var isEmpty bool
	group := getStringList(ts, argumentValues, "group", &isEmpty)
	start, end := getStartEnd(argumentValues)
	l := G.NewListValue()
	if start >= end {
		return l
	}
	t := doMissingEnds(group, start, end, isEmpty)
	e := t.Next(nil)
	for e != nil {
		m, ok := e.Val().(*membership); M.Assert(ok, 100)
		_, _, h, _, _, _, b := B.IdUidComplete(m.uid); M.Assert(b, 101)
		l.Append(G.Wrap(h))
		e = t.Next(e)
	}
	return l
} //missEndsR

func certEndsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var isEmpty bool
	group := getStringList(ts, argumentValues, "group", &isEmpty)
	start, end := getStartEnd(argumentValues)
	l := G.NewListValue()
	if start >= end {
		return l
	}
	var (v G.Value; missingIncluded bool)
	if G.GetValue(argumentValues, "missingIncluded", &v) {
		switch v := v.(type) {
		case *G.BooleanValue:
			missingIncluded = v.Boolean
		default:
			M.Halt(v, 100)
		}
	} else {
		missingIncluded = true
	}
	t := doCertifsEnds(group, start, end, missingIncluded, isEmpty)
	e := t.Next(nil)
	for e != nil {
		m, ok := e.Val().(*membership); M.Assert(ok, 100)
		_, _, h, _, _, _, b := B.IdUidComplete(m.uid); M.Assert(b, 101)
		l.Append(G.Wrap(h))
		e = t.Next(e)
	}
	return l
} //certEndsR

func fixFieldResolvers (ts G.TypeSystem) {
	ts.FixFieldResolver("Query", "memEnds", memEndsR)
	ts.FixFieldResolver("Query", "missEnds", missEndsR)
	ts.FixFieldResolver("Query", "certEnds", certEndsR)
	ts.FixFieldResolver("Subscription", "memEnds", memEndsR)
	ts.FixFieldResolver("Subscription", "missEnds", missEndsR)
	ts.FixFieldResolver("Subscription", "certEnds", certEndsR)
} //fixFieldResolvers

func fixStreamResolvers (ts G.TypeSystem) {
	ts.FixStreamResolver("memEnds", memStreamResolver)
	ts.FixStreamResolver("missEnds", missStreamResolver)
	ts.FixStreamResolver("certEnds", certStreamResolver)
}

func init () {
	ts := GQ.TS()
	fixFieldResolvers(ts)
	fixStreamResolvers(ts)
}
