/* 
WotWizard

Copyright (C) 2017-2020 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package identities

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	BA	"git.duniter.org/gerard94/wotwizard/basic"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	G	"git.duniter.org/gerard94/util/graphQL"
	GQ	"git.duniter.org/gerard94/wotwizard/gqlReceiver"
	IS	"git.duniter.org/gerard94/wotwizard/identitySearchList"
	M	"git.duniter.org/gerard94/util/misc"
	S	"git.duniter.org/gerard94/util/smallSets"
	S3	"git.duniter.org/gerard94/util/sets3"
	SB	"git.duniter.org/gerard94/wotwizard/sandbox"
		/*
		"fmt"
		*/

)

type (
	
	// Used to sort identities by 'uid' or by 'pubkey' in 'identitiesList'
	uidPubId struct {
		uid,
		pub string
		id B.Hash
	}
	
	// Used as error marker in 'filterGroup'
	idIndex struct {
		uid string
		id B.Hash
		index int
	}
	
	idIndexMult idIndex

)

var (
	
	bcStatuses = S.MakeSet(IS.Revoked, IS.Missing, IS.Member)
	sbStatuses = S.MakeSet(IS.Newcomer)

)

// Is the identity described by 'member' or not and the expiration date 'exp' in the set 'set' of statuses?
func filter (set S.Set, member bool, exp int64) bool {
	var status int
	if member {
		status = IS.Member
	} else if exp == BA.Revoked {
		status = IS.Revoked
	} else {
		status = IS.Missing
	}
	return set.In(status)
} //filter

// Comparison function of the 'avl.Comparer' '*uidPubId'
func (i1 *uidPubId) Compare (i2 A.Comparer) A.Comp {
	ii2, ok := i2.(*uidPubId); M.Assert(ok, 100)
	switch comp := BA.CompP(i1.uid, ii2.uid); comp {
	case A.Lt, A.Gt:
		return comp
	default:
		if i1.pub < ii2.pub {
			return A.Lt
		}
		if i1.pub > ii2.pub {
			return A.Gt
		}
		if i1.id < ii2.id {
			return A.Lt
		}
		if i1.id > ii2.id {
			return A.Gt
		}
	}
	return A.Eq
} //Compare

// Comparison function of the 'avl.Comparer' '*idIndex'
func (i1 *idIndex) Compare (i2 A.Comparer) A.Comp {
	ii2, ok := i2.(*idIndex); M.Assert(ok, 100)
	return BA.CompP(i1.uid, ii2.uid)
} //Compare

// Comparison function of the 'avl.Comparer' '*idIndexMult'
func (i1 *idIndexMult) Compare (i2 A.Comparer) A.Comp {
	ii2, ok := i2.(*idIndexMult); M.Assert(ok, 100)
	switch comp := BA.CompP(i1.uid, ii2.uid); comp {
	case A.Lt, A.Gt:
		return comp
	default:
		if i1.index < ii2.index {
			return A.Lt
		}
		if i1.index > ii2.index {
			return A.Gt
		}
		return A.Eq
	}
} //Compare

// Implementation of the graphQL field 'Query.filterGroup'
func filterGroup (group *G.ListValue, statuses S.Set) (selected, others, unknown, duplicate *A.Tree) {
	
	selected = A.New()
	others = A.New()
	unknown = A.New()
	duplicate = A.New()
	
	dispatch := func (t *A.Tree, uid string, h B.Hash, index int) {
		el := &idIndex{uid: uid, id: h, index: index}
		_, found, _ := t.SearchIns(el)
		if found {
			elM := new(idIndexMult)
			*elM = idIndexMult(*el)
			duplicate.SearchIns(elM)
		}
	} //dispatch
	
	//filterGroup
	inBCSet := statuses.Inter(bcStatuses)
	inSB := !statuses.Inter(sbStatuses).IsEmpty()
	i := 0
	for e := group.First(); e != nil; e = group.Next(e) {
		sv, ok := e.Value.(*G.StringValue); M.Assert(ok, 100)
		uid := sv.String.S
		found := false
		var (member bool; hash B.Hash; exp int64)
		_, member, hash, _, _, exp, found = B.IdUidComplete(uid)
		if found {
			if filter(inBCSet, member, exp) {
				dispatch(selected, uid, hash, i)
			} else {
				dispatch(others, uid, hash, i)
			}
		}
		el := SB.IdPosUid(uid)
		uid2, hash, ok := SB.IdNextUid(false, &el)
		fd := ok && BA.CompP(uid2, uid) == BA.Eq
		if fd {
			_, b := B.IdUid(uid)
			fd = !b
		}
		if fd {
			if inSB {
				dispatch(selected, uid, hash, i)
			} else {
				dispatch(others, uid, hash, i)
			}
		}
		found = found || fd
		if !found {
			dispatch(unknown, uid, "", i)
		}
		i++
	}
	return
} //filterGroup

// Implementation of the graphQL field 'Query.identities'
func identitiesList (group *S3.Set, groupNull bool, statuses S.Set, sortedByPubkey bool, from, to string) G.Value {
	InBCSet := statuses.Inter(bcStatuses)
	inBC := !InBCSet.IsEmpty()
	inSB := !statuses.Inter(sbStatuses).IsEmpty()
	t := A.New()
	if groupNull {
		if inBC {
			if sortedByPubkey {
				fromP := B.Pubkey(from)
				toP := B.Pubkey(to)
				ir := B.IdPosPubkey(fromP)
				pubkey, ok := B.IdNextPubkey(false, &ir)
				for ok && (toP == "" || pubkey < toP)  {
					_, member, hash, _, _, exp, b := B.IdPubComplete(pubkey); M.Assert(b, 100)
					if filter(InBCSet, member, exp) {
						t.SearchIns(&uidPubId{pub: string(pubkey), id: hash})
					}
					pubkey, ok = B.IdNextPubkey(false, &ir)
				}
			} else {
				ir := B.IdPosUid(from)
				uid, ok := B.IdNextUid(false, &ir)
				for ok && (to == "" || BA.CompP(uid, to) == BA.Lt) {
					_, member, hash, _, _, exp, b := B.IdUidComplete(uid); M.Assert(b, 101)
					if filter(InBCSet, member, exp) {
						t.SearchIns(&uidPubId{uid: uid, id: hash})
					}
					uid, ok = B.IdNextUid(false, &ir)
				}
			}
		}
		if inSB {
			if sortedByPubkey {
				fromP := B.Pubkey(from)
				toP := B.Pubkey(to)
				el := SB.IdPosPubkey(fromP)
				p, hash, ok := SB.IdNextPubkey(false, &el)
				for ok && (toP == "" || p < toP)  {
					if _, ok := B.IdHash(hash); !ok {
						t.SearchIns(&uidPubId{pub: string(p), id: hash})
					}
					p, hash, ok = SB.IdNextPubkey(false, &el)
				}
			} else {
				el := SB.IdPosUid(from)
				uid, hash, ok := SB.IdNextUid(false, &el)
				for ok && (to == "" || BA.CompP(uid, to) == BA.Lt) {
					if _, ok := B.IdHash(hash); !ok {
						t.SearchIns(&uidPubId{uid: uid, id: hash})
					}
					uid, hash, ok = SB.IdNextUid(false, &el)
				}
			}
		}
	} else {
		it := group.Attach()
		uid, ok := it.FirstE()
		for ok {
			found := false
			if inBC {
				var (pubkey B.Pubkey; member bool; hash SB.Hash; exp int64)
				pubkey, member, hash, _, _, exp, found = B.IdUidComplete(uid)
				if found && filter(InBCSet, member, exp) {
					if sortedByPubkey {
						fromP := B.Pubkey(from)
						toP := B.Pubkey(to)
						if fromP <= pubkey && (toP == "" || pubkey < toP) {
							t.SearchIns(&uidPubId{pub: string(pubkey), id: hash})
						}
					} else if BA.CompP(from, uid) <= BA.Eq && (to == "" || BA.CompP(uid, to) == BA.Lt) {
						t.SearchIns(&uidPubId{uid: uid, id: hash})
					}
				}
			}
			if inSB {
				el := SB.IdPosUid(uid)
				uid2, hash, ok := SB.IdNextUid(false, &el)
				for ok && BA.CompP(uid2, uid) == BA.Eq {
					found = true
					if _, ok := B.IdHash(hash); !ok {
						if sortedByPubkey {
							fromP := B.Pubkey(from)
							toP := B.Pubkey(to)
							_, p, _, _, _, ok := SB.IdHash(hash); M.Assert(ok, 102)
							if fromP <= p && (toP == "" || p < toP) {
								t.SearchIns(&uidPubId{pub: string(p), id: hash})
							}
						} else if BA.CompP(from, uid) <= BA.Eq && (to == "" || BA.CompP(uid, to) == BA.Lt) {
							t.SearchIns(&uidPubId{uid: uid, id: hash})
						}
					}
					uid2, hash, ok = SB.IdNextUid(false, &el)
				}
			}
			if !found {
				return G.MakeNullValue()
			}
			uid, ok = it.NextE()
		}
	}
	l := G.NewListValue()
	e := t.Next(nil)
	for e != nil {
		id, ok := e.Val().(*uidPubId); M.Assert(ok, 103)
		l.Append(G.Wrap(id.id))
		e = t.Next(e)
	}
	return l
} //identitiesList

// Transcoding status
func takeStatus (enum *G.EnumValue) (status int) {
	switch enum.Enum.S {
	case "REVOKED":
		status = IS.Revoked
	case "MISSING":
		status = IS.Missing
	case "MEMBER":
		status = IS.Member
	case "NEWCOMER":
		status = IS.Newcomer
	default:
		M.Halt(enum.Enum.S, 100)
	}
	return
} //takeStatus

// Get argument 'group' as a '*G.ListValue'
func getGroupList (argumentValues *A.Tree) G.Value {
	var v G.Value
	ok := G.GetValue(argumentValues, "group", &v)
	if !ok {
		return G.MakeNullValue()
	}
	switch group := v.(type) {
	case *G.ListValue:
		return group
	case *G.NullValue:
		return group
	default:
		M.Halt(v, 100)
		return nil
	}
} //getGroupList

// Get argument 'group' as a '*S3.Set' ('groupNull' true if 'group' is null)
func getGroupSet (argumentValues *A.Tree) (group *S3.Set, groupNull bool) {
	set := S3.NewSet()
	l := getGroupList(argumentValues)
	switch l := l.(type) {
	case *G.ListValue:
		e := l.First()
		for e != nil {
			sv, ok := e.Value.(*G.StringValue); M.Assert(ok, 100)
			b := set.Incl(sv.String.S); M.Assert(b, 101)
			e = l.Next(e)
		}
		return set, false
	case *G.NullValue:
	}
	return set, true
} //getGroupSet

// Get argument 'status_list' as a 'S.Set'
func getStatusList (argumentValues *A.Tree) S.Set {
	var v G.Value
	ok := G.GetValue(argumentValues, "status_list", &v)
	if !ok {
		return S.NewSet()
	}
	switch v := v.(type) {
	case *G.ListValue:
		statusSet := S.NewSet()
		for l := v.First(); l != nil; l = v.Next(l) {
			(&statusSet).Incl(takeStatus(l.Value.(*G.EnumValue)))
		}
		return statusSet
	default:
		M.Halt(v, 101)
		return S.NewSet()
	}
} //getStatusList

// Get argument 'sortedBy' (true if 'PUBKEY')
func getOrder (argumentValues *A.Tree) bool {
	var v G.Value
	ok := G.GetValue(argumentValues, "sortedBy", &v)
	M.Assert(ok, 100)
	switch v := v.(type) {
	case *G.EnumValue:
		return v.Enum.S == "PUBKEY"
	default:
		M.Halt(v, 101)
		return false
	}
} //getOrder

// Get arguments 'start' and 'end'
func getLimits (argumentValues *A.Tree) (from, to string) {
	
	get := func (name string) string {
		var v G.Value
		ok := G.GetValue(argumentValues, name, &v)
		M.Assert(ok, 100)
		switch v := v.(type) {
		case *G.StringValue:
			return v.String.S
		default:
			M.Halt(v, 100)
			return ""
		}
	} //get
	
	//getLimits
	from = get("start")
	to = get("end")
	return
} //getLimits

// G.Path -> *GListValue
func path2List (path G.Path) *G.ListValue {
	l := G.NewListValue()
	for path != nil {
		switch p := path.(type) {
		case *G.PathString:
			l.Append(G.Wrap(p))
		case *G.PathNb:
			l.Append(G.Wrap(p))
		}
		path = path.Next()
	}
	return l
} //path2List

// *A.Tree (*idIndex) -> *GListValue
func tree2IdIndList (t *A.Tree) *G.ListValue {
	l := G.NewListValue()
	for e := t.Next(nil); e != nil; e = t.Next(e) {
		idI, ok := e.Val().(*idIndex)
		if ok {
			l.Append(G.Wrap(idI))
		} else {
			idM, ok := e.Val().(*idIndexMult); M.Assert(ok, 100)
			l.Append(G.Wrap(idM))
		}
	}
	return l
} //tree2IdIndList

func filterGroupR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	group := getGroupList(argumentValues).(*G.ListValue)
	statuses := getStatusList(argumentValues)
	selected, others, unknown, duplicate := filterGroup(group, statuses)
	return G.Wrap(selected, others, unknown, duplicate)
} //filterGroupR

func gfSelR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 0)
	switch t := v.(type) {
	case *A.Tree:
		return tree2IdIndList(t)
	default:
		M.Halt(t, 100)
		return nil
	}
} //gfSelR

func gfOthersR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 1)
	switch t := v.(type) {
	case *A.Tree:
		return tree2IdIndList(t)
	default:
		M.Halt(t, 100)
		return nil
	}
} //gfOthersR

func gfUnknownR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 2)
	switch t := v.(type) {
	case *A.Tree:
		return tree2IdIndList(t)
	default:
		M.Halt(t, 100)
		return nil
	}
} //gfUnknownR

func gfDupR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 3)
	switch t := v.(type) {
	case *A.Tree:
		return tree2IdIndList(t)
	default:
		M.Halt(t, 100)
		return nil
	}
} //gfDupR

func griIdR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 0)
	switch idI := v.(type) {
	case *idIndex:
		return G.Wrap(idI.id)
	default:
		M.Halt(idI, 100)
		return nil
	}
} //griIdR

func grsUidR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 0)
	switch idI := v.(type) {
	case *idIndex:
		return G.MakeStringValue(idI.uid)
	case *idIndexMult:
		return G.MakeStringValue(idI.uid)
	default:
		M.Halt(idI, 100)
		return nil
	}
} //grsUidR

func grIndR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 0)
	switch idI := v.(type) {
	case *idIndex:
		return G.MakeIntValue(idI.index)
	case *idIndexMult:
		return G.MakeIntValue(idI.index)
	default:
		M.Halt(idI, 100)
		return nil
	}
} //grIndR

func identitiesR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	group, groupNull := getGroupSet(argumentValues)
	order := getOrder(argumentValues)
	from, to := getLimits(argumentValues)
	statuses := getStatusList(argumentValues)
	return identitiesList(group, groupNull, statuses, order, from, to)
} //identitiesR

func idSearchR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var (v G.Value; hint string; statusList IS.StatusList)
	ok := G.GetValue(argumentValues, "with", &v)
	M.Assert(ok, 100)
	switch v := v.(type) {
	case *G.InputObjectValue:
		var vv G.Value
		ok := G.GetObjectValueInputField(v, "hint", &vv)
		M.Assert(ok, 101)
		switch vv := vv.(type) {
		case *G.StringValue:
			hint = vv.String.S
		default:
			M.Halt(vv, 102)
		}
		ok = G.GetObjectValueInputField(v, "status_list", &vv)
		M.Assert(ok, 103)
		switch vv := vv.(type) {
		case *G.ListValue:
			statusList = make(IS.StatusList, vv.Len())
			i := 0
			for l := vv.First(); l != nil; l = vv.Next(l) {
				statusList[i] = takeStatus(l.Value.(*G.EnumValue))
				i++
			}
		default:
			M.Halt(vv, 104)
		}
	default:
		M.Halt(105)
	}
	nR, nM, nA, nF, ids := IS.Find(hint, statusList)
	return G.Wrap(nR, nM, nA, nF, ids)
} //idSearchR

func idFromHashR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	var v G.Value
	ok := G.GetValue(argumentValues, "hash", &v)
	if !ok {
		return G.MakeNullValue() 
	}
	switch h := v.(type) {
	case *G.StringValue:
		hash := B.Hash(h.String.S)
		_, ok := B.IdHash(hash)
		if !ok {
			_, _, _, _, _, ok = SB.IdHash(hash)
		}
		if !ok {
			return G.MakeNullValue()
		}
		return G.Wrap(hash)
	default:
		M.Halt(h, 100)
		return nil
	}
} //idFromHashR

func idSearchOutputNRR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch nR := G.Unwrap(rootValue, 0).(type) {
	case int:
		return G.MakeIntValue(nR)
	default:
		M.Halt(nR, 100)
		return nil
	}
} //idSearchOutputNRR

func idSearchOutputNMR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch nM := G.Unwrap(rootValue, 1).(type) {
	case int:
		return G.MakeIntValue(nM)
	default:
		M.Halt(nM, 100)
		return nil
	}
} //idSearchOutputNMR

func idSearchOutputNAR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch nA := G.Unwrap(rootValue, 2).(type) {
	case int:
		return G.MakeIntValue(nA)
	default:
		M.Halt(nA, 100)
		return nil
	}
} //idSearchOutputNAR

func idSearchOutputNFR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch nF := G.Unwrap(rootValue, 3).(type) {
	case int:
		return G.MakeIntValue(nF)
	default:
		M.Halt(nF, 100)
		return nil
	}
} //idSearchOutputNFR

func idSearchOutputIdsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch ids := G.Unwrap(rootValue, 4).(type) {
	case *A.Tree:
		l := G.NewListValue()
		e := ids.Next(nil)
		for e != nil {
			l.Append(G.Wrap(e.Val().(*IS.IdET).Hash))
			e = ids.Next(e)
		}
		return l
	default:
		M.Halt(ids, 100)
		return nil
	}
} //idSearchOutputIdsR

func identityPubkeyR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, _, _, ok := IS.Get(hash); M.Assert(ok, 100)
		return G.MakeStringValue(string(pub))
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityPubkeyR

func identityUidR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		uid, _, _, _, _, _, _, _, ok := IS.Get(hash)
		if !ok {
			return G.MakeNullValue()
		}
		return G.MakeStringValue(uid)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityUidR

func identityHashR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		return G.MakeStringValue(string(hash))
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityHashR

func identityStatusR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, _, exp, inBC, member, ok := IS.Get(hash); M.Assert(ok, 100)
		var s string
		if !inBC {
			s = "NEWCOMER"
		} else {
			if member {
				s = "MEMBER"
			} else if exp != BA.Revoked {
				s = "MISSING"
			} else {
				s = "REVOKED"
			}
		}
		return G.MakeEnumValue(s)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityStatusR

func identityMPR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, _, mp := SB.IdHash(hash)
		return G.MakeBooleanValue(mp)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityMPR

func identityMPBR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, bnb, _, mp := SB.IdHash(hash)
		if mp {
			return G.Wrap(bnb)
		} else {
			return G.MakeNullValue()
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityMPBR

func identityMPLDR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, exp, mp := SB.IdHash(hash)
		if mp {
			return G.MakeInt64Value(exp)
		} else {
			return G.MakeNullValue()
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityMPLDR

func identityIdWrittenBlockR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, block, _, _, _, _, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if block >= 0 {
			return G.Wrap(block)
		} else {
			return G.MakeNullValue()
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityIdWrittenBlockR

func identityLastApplicationR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, app, _, _, _, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if app >= 0 {
			return G.Wrap(app)
		} else {
			return G.MakeNullValue()
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityLastApplicationR

func identityPendingApplicationR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, app, _, _, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if app >= 0 {
			return G.Wrap(app)
		} else {
			return G.MakeNullValue()
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityPendingApplicationR

func identityLimitDateR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, _, exp, _, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if exp == BA.Revoked {
			return G.MakeNullValue()
		}
		return G.MakeInt64Value(M.Abs64(exp))
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityLimitDateR

func identityCertsLimitR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, p, _, _, _, exp, inBC, _, b := IS.Get(hash); M.Assert(b, 100)
		var pos B.CertPos
		if !inBC || exp == BA.Revoked || !B.CertToByExp(p, &pos) {
			return G.MakeNullValue()
		}
		n := int(B.Pars().SigQty)
		if pos.CertPosLen() < n {
			return G.MakeNullValue()
		}
		var from, to B.Pubkey
		for j := 1; j <= n; j++ {
			from, to, b = pos.CertNextPos(); M.Assert(b, 102)
		}
		_, exp, b = B.Cert(from, to); M.Assert(b, 103)
		return G.MakeInt64Value(exp)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityCertsLimitR

func identityLeavingR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, _, _, _, _, exp, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		if !inBC || exp == BA.Revoked {
			return G.MakeNullValue()
		}
		return G.MakeBooleanValue(exp < 0)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityLeavingR

func identitySentryR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, _, member, ok := IS.Get(hash); M.Assert(ok, 100)
		if member {
			return G.MakeBooleanValue(B.IsSentry(pub))
		} else {
			return G.MakeBooleanValue(false)
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identitySentryR

func identityRecCertsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		certifiers, _ := IS.RecCerts(hash, pub, inBC, true)
		l := G.NewListValue()
		e := certifiers.Next(nil)
		for e != nil {
			idE := e.Val().(*IS.IdET)
			l.Append(G.Wrap(idE.Hash, hash, idE.Status == IS.Newcomer)) // Status == newcomer means certification is future
			e = certifiers.Next(e)
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityRecCertsR

func identitySentCertsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		_, _, certified := IS.SentCerts(hash, pub, inBC, true)
		l := G.NewListValue()
		e := certified.Next(nil)
		for e != nil {
			idE := e.Val().(*IS.IdET)
			l.Append(G.Wrap(hash, idE.Hash, idE.Status == IS.Newcomer)) // Status == newcomer means certification is future
			e = certified.Next(e)
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identitySentCertsR

// identitySentCertsNbR returns the number of sent certifications currently valid in blockchain
func identitySentCertsNbR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		nb, _, _ := IS.SentCerts(hash, pub, inBC, false)
		return G.MakeIntValue(nb)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identitySentCertsNbR

func identityAllRecCertsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		l := G.NewListValue()
		pubkey, inBC := B.IdHash(hash)
		if inBC {
			uid, b := B.IdPub(pubkey); M.Assert(b, 100)
			for _, uid := range B.AllCertifiers(uid) {
				_, _, h, _, _, _, b := B.IdUidComplete(uid); M.Assert(b, 101)
				l.Append(G.Wrap(h)) // Status == newcomer means certification is future
			}
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityAllRecCertsR

func identityAllSentCertsR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		l := G.NewListValue()
		pubkey, inBC := B.IdHash(hash)
		if inBC {
			uid, b := B.IdPub(pubkey); M.Assert(b, 100)
			for _, uid := range B.AllCertified(uid) {
				_, _, h, _, _, _, b := B.IdUidComplete(uid); M.Assert(b, 101)
				l.Append(G.Wrap(h)) // Status == newcomer means certification is future
			}
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityAllSentCertsR

func identityAllRecCertsIOR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		l := G.NewListValue()
		pubkey, inBC := B.IdHash(hash)
		if inBC {
			uid, b := B.IdPub(pubkey); M.Assert(b, 100)
			for _, ch := range B.AllCertifiersIO(uid) {
				_, _, h, _, _, _, b := B.IdUidComplete(ch.Uid); M.Assert(b, 101)
				l.Append(G.Wrap(h, ch.Hist)) // Status == newcomer means certification is future
			}
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityAllRecCertsIOR

func identityAllSentCertsIOR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		l := G.NewListValue()
		pubkey, inBC := B.IdHash(hash)
		if inBC {
			uid, b := B.IdPub(pubkey); M.Assert(b, 100)
			for _, ch := range B.AllCertifiedIO(uid) {
				_, _, h, _, _, _, b := B.IdUidComplete(ch.Uid); M.Assert(b, 101)
				l.Append(G.Wrap(h, ch.Hist)) // Status == newcomer means certification is future
			}
		}
		return l
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityAllSentCertsIOR

func certHistIdR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		return G.Wrap(hash)
	default:
		M.Halt(hash, 100)
		return nil
	}
} //certHistIdR

func certHistHistR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hist := G.Unwrap(rootValue, 1).(type) {
	case B.CertEvents:
		l := G.NewListValue()
		for _, ce := range hist {
			l.Append(G.Wrap(ce.InOut, ce.Block))
		}
		return l
	default:
		M.Halt(hist, 100)
		return nil
	}
} //certHistHistR

func certEventInR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch b := G.Unwrap(rootValue, 0).(type) {
	case bool:
		return G.MakeBooleanValue(b)
	default:
		M.Halt(b, 100)
		return nil
	}
} //certEventInR

func certEventBlockR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch block := G.Unwrap(rootValue, 1).(type) {
	case int32:
		return G.Wrap(block)
	default:
		M.Halt(block, 100)
		return nil
	}
} //certEventBlockR

func identityDistanceR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, member, ok := IS.Get(hash); M.Assert(ok, 100)
		_, certifiers := IS.RecCerts(hash, pub, inBC, false)
		num, denom, ratio, distOk := IS.NotTooFar(pub, member, certifiers)
		return G.Wrap(G.Wrap(num, denom, ratio), distOk)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityDistanceR

func identityDistanceER (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, member, ok := IS.Get(hash); M.Assert(ok, 100)
		_, certifiers := IS.RecCerts(hash, pub, inBC, true)
		num, denom, ratio, distOk := IS.NotTooFar(pub, member, certifiers)
		return G.Wrap(G.Wrap(num, denom, ratio), distOk)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityDistanceER

func identityQualityR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, member, ok := IS.Get(hash); M.Assert(ok, 100)
		_, certifiers := IS.RecCerts(hash, pub, inBC, false)
		num, denom, ratio := IS.CalcQuality(pub, member, certifiers)
		return G.Wrap(num, denom, ratio)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityQualityR

func identityQualityER (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, member, ok := IS.Get(hash); M.Assert(ok, 100)
		_, certifiers := IS.RecCerts(hash, pub, inBC, true)
		num, denom, ratio := IS.CalcQuality(pub, member, certifiers)
		return G.Wrap(num, denom, ratio)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityQualityER

func identityCentralityR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, inBC, _, ok := IS.Get(hash); M.Assert(ok, 100)
		return G.MakeFloat64Value(IS.CalcCentrality(pub, inBC) * 100)
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityCentralityR

func identityMinDateR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, _, member, ok := IS.Get(hash); M.Assert(ok, 100)
		date, _ := IS.FixCertNextDate(member, pub)
		if date < 0 {
			return G.MakeNullValue()
		} else {
			return G.MakeInt64Value(date)
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityMinDateR

func identityMinDatePassedR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch hash := G.Unwrap(rootValue, 0).(type) {
	case B.Hash:
		_, pub, _, _, _, _, _, member, ok := IS.Get(hash); M.Assert(ok, 100)
		date, passed := IS.FixCertNextDate(member, pub)
		if date < 0 {
			return G.MakeNullValue()
		} else {
			return G.MakeBooleanValue(passed)
		}
	case *G.NullValue:
		return hash
	default:
		M.Halt(hash, 100)
		return nil
	}
} //identityMinDatePassedR

func DistanceValR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	v := G.Unwrap(rootValue, 0)
	frac, ok := v.(*G.OutputObjectValue); M.Assert(ok, frac, 100)
	return frac
} //DistanceValR

func DistanceOkR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch ok := G.Unwrap(rootValue, 1).(type) {
	case bool:
		return G.MakeBooleanValue(ok)
	default:
		M.Halt(ok, 100)
		return nil
	}
} //DistanceOkR

func fracNumR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch num := G.Unwrap(rootValue, 0).(type) {
	case int:
		return G.MakeIntValue(num)
	default:
		M.Halt(num, 100)
		return nil
	}
} //fracNumR

func fracDenomR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch denom := G.Unwrap(rootValue, 1).(type) {
	case int:
		return G.MakeIntValue(denom)
	default:
		M.Halt(denom, 100)
		return nil
	}
} //fracDenomR

func fracRatioR (ts G.TypeSystem, rootValue *G.OutputObjectValue, argumentValues *A.Tree) G.Value {
	switch ratio := G.Unwrap(rootValue, 2).(type) {
	case float64:
		return G.MakeFloat64Value(ratio * 100)
	default:
		M.Halt(ratio, 100)
		return nil
	}
} //fracRatioR

func fixFieldResolvers (ts G.TypeSystem) {
	ts.FixFieldResolver("Query", "filterGroup", filterGroupR)
	ts.FixFieldResolver("Query", "identities", identitiesR)
	ts.FixFieldResolver("Query", "idSearch", idSearchR)
	ts.FixFieldResolver("Query", "idFromHash", idFromHashR)
	
	ts.FixFieldResolver("IdSearchOutput", "revokedNb", idSearchOutputNRR)
	ts.FixFieldResolver("IdSearchOutput", "missingNb", idSearchOutputNMR)
	ts.FixFieldResolver("IdSearchOutput", "memberNb", idSearchOutputNAR)
	ts.FixFieldResolver("IdSearchOutput", "newcomerNb", idSearchOutputNFR)
	ts.FixFieldResolver("IdSearchOutput", "ids", idSearchOutputIdsR)
	
	ts.FixFieldResolver("Identity", "pubkey", identityPubkeyR)
	ts.FixFieldResolver("Identity", "uid", identityUidR)
	ts.FixFieldResolver("Identity", "hash", identityHashR)
	ts.FixFieldResolver("Identity", "status", identityStatusR)
	ts.FixFieldResolver("Identity", "membership_pending", identityMPR)
	ts.FixFieldResolver("Identity", "membership_pending_block", identityMPBR)
	ts.FixFieldResolver("Identity", "membership_pending_limitDate", identityMPLDR)
	ts.FixFieldResolver("Identity", "id_written_block", identityIdWrittenBlockR)
	ts.FixFieldResolver("Identity", "lastApplication", identityLastApplicationR)
	ts.FixFieldResolver("Identity", "pendingApplication", identityPendingApplicationR)
	ts.FixFieldResolver("Identity", "limitDate", identityLimitDateR)
	ts.FixFieldResolver("Identity", "certsLimit", identityCertsLimitR)
	ts.FixFieldResolver("Identity", "isLeaving", identityLeavingR)
	ts.FixFieldResolver("Identity", "sentry", identitySentryR)
	ts.FixFieldResolver("Identity", "received_certifications", identityRecCertsR)
	ts.FixFieldResolver("Identity", "sent_certifications", identitySentCertsR)
	ts.FixFieldResolver("Identity", "sent_certifications_number", identitySentCertsNbR)
	ts.FixFieldResolver("Identity", "all_certifiers", identityAllRecCertsR)
	ts.FixFieldResolver("Identity", "all_certified", identityAllSentCertsR)
	ts.FixFieldResolver("Identity", "all_certifiersIO", identityAllRecCertsIOR)
	ts.FixFieldResolver("Identity", "all_certifiedIO", identityAllSentCertsIOR)
	ts.FixFieldResolver("Identity", "distance", identityDistanceR)
	ts.FixFieldResolver("Identity", "distanceE", identityDistanceER)
	ts.FixFieldResolver("Identity", "quality", identityQualityR)
	ts.FixFieldResolver("Identity", "qualityE", identityQualityER)
	ts.FixFieldResolver("Identity", "centrality", identityCentralityR)
	ts.FixFieldResolver("Identity", "minDate", identityMinDateR)
	ts.FixFieldResolver("Identity", "minDatePassed", identityMinDatePassedR)
	
	ts.FixFieldResolver("GroupFilter", "selected", gfSelR)
	ts.FixFieldResolver("GroupFilter", "others", gfOthersR)
	ts.FixFieldResolver("GroupFilter", "unknown", gfUnknownR)
	ts.FixFieldResolver("GroupFilter", "duplicate", gfDupR)
	
	ts.FixFieldResolver("GroupId", "id", griIdR)
	ts.FixFieldResolver("GroupId", "index", grIndR)
	
	ts.FixFieldResolver("GroupString", "uid", grsUidR)
	ts.FixFieldResolver("GroupString", "index", grIndR)
	
	ts.FixFieldResolver("CertHist", "id", certHistIdR)
	ts.FixFieldResolver("CertHist", "hist", certHistHistR)
	
	ts.FixFieldResolver("CertEvent", "in", certEventInR)
	ts.FixFieldResolver("CertEvent", "block", certEventBlockR)
	
	ts.FixFieldResolver("Distance", "value", DistanceValR)
	ts.FixFieldResolver("Distance", "dist_ok", DistanceOkR)
	
	ts.FixFieldResolver("Fraction", "num", fracNumR)
	ts.FixFieldResolver("Fraction", "denom", fracDenomR)
	ts.FixFieldResolver("Fraction", "ratio", fracRatioR)
} //fixFieldResolvers

func init () {
	fixFieldResolvers(GQ.TS())
} //init
