#!/bin/bash

# Intercept SIGTERM and send SIGTERM to wwServer and tail
trap 'kill $WW_SERVER_PID $TAIL_PID' SIGTERM SIGINT

# Inject the configuration
mkdir -p rsrc/duniter
echo "/app/data/duniter_default/wotwizard-export.db" > rsrc/duniter/init.txt
echo '"0.0.0.0:8080"' > rsrc/duniter/serverAddress.txt
touch rsrc/duniter/log.txt

# Start the server and tail the log
./wwServer > /dev/null &
WW_SERVER_PID=$!
tail -f rsrc/duniter/log.txt &
TAIL_PID=$!
wait $WW_SERVER_PID
WW_SERVER_EXIT_CODE=$?
kill $TAIL_PID
exit $WW_SERVER_EXIT_CODE
