/* 
WotWizard

Copyright (C) 2017-2020 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package identitySearchList

// À corriger dans certs

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	B	"git.duniter.org/gerard94/wotwizard/blockchain"
	BA	"git.duniter.org/gerard94/wotwizard/basic"
	C	"git.duniter.org/gerard94/wotwizard/centralities"
	M	"git.duniter.org/gerard94/util/misc"
	S	"git.duniter.org/gerard94/util/smallSets"
	SB	"git.duniter.org/gerard94/wotwizard/sandbox"
		"strings"

)

const (
	
	Revoked = iota
	Missing
	Member
	Newcomer

)

type (
	
	StatusList []int
	
	IdET struct {
		uid string
		Hash B.Hash
		Status int
		limit int64
	}
	
	expSort struct {
		exp []int64
	}

)

func (e *expSort) Less (i, j int) bool {
	return e.exp[i] < e.exp[j]
} //Less

func (e *expSort) Swap (i, j int) {
	exp := e.exp[i]; e.exp[i] = e.exp[j]; e.exp[j] = exp
} //Swap

func (i1 *IdET) Compare (i2 A.Comparer) A.Comp {
	ii2 := i2.(*IdET)
	b := BA.CompP(i1.uid, ii2.uid)
	if b != A.Eq {
		return b
	}
	if i1.Status == Newcomer && ii2.Status != Newcomer {
		return A.Lt
	}
	if i1.Status != Newcomer && ii2.Status == Newcomer {
		return A.Gt
	}
	if i1.Hash < ii2.Hash {
		return A.Lt
	}
	if i1.Hash > ii2.Hash {
		return A.Gt
	}
	return A.Eq
} //Compare

func getStatus (inBC, active bool, exp int64) (status int) {
	if active {
		status = Member
	} else if exp == BA.Revoked {
		status = Revoked
	} else if inBC {
		status = Missing
	} else {
		status = Newcomer
	}
	return
} //getStatus

func Find (hint string, sl StatusList) (nR, nM, nA, nF int, ids *A.Tree) { //IdET
	
	getFlags := func (sl StatusList) S.Set {
		set := S.MakeSet()
		for _, s := range sl {
			set.Incl(s)
		}
		return set
	} //getFlags
	
	incNbs :=func (status int, nR, nM, nA, nF *int) {
		switch status {
		case Revoked:
			*nR++
		case Missing:
			*nM++
		case Member:
			*nA++
		case Newcomer:
			*nF++
		}
	} //incNbs
	
	//Find
	asked := getFlags(sl)
	ids = A.New() //IdET
	set := A.New() //IdET
	hintD := BA.ToDown(hint)
	ir := B.IdPosUid(hintD)
	nR = 0; nM = 0; nA = 0; nF = 0
	uid, okL := B.IdNextUid(false, &ir)
	for okL {
		ok := BA.Prefix(hintD, BA.ToDown(uid))
		var (active bool; hash B.Hash; exp int64)
		if ok {
			_, active, hash, _, _, exp, ok = B.IdUidComplete(uid); M.Assert(ok, 100)
			_, b, _ := set.SearchIns(&IdET{uid: uid, Hash: hash}); M.Assert(!b, 101)
			status := getStatus(true, active, exp)
			incNbs(status, &nR, &nM, &nA, &nF)
			if asked.In(status) {
				_, b, _ := ids.SearchIns(&IdET{uid: uid, Hash: hash, Status: status}); M.Assert(!b, 102)
			}
		}
		uid, okL = B.IdNextUid(false, &ir)
	}
	if len(hint) <= B.PubkeyLen {
		ir := B.IdPosPubkey(B.Pubkey(hint))
		p, okL := B.IdNextPubkey(false, &ir)
		for okL {
			ok := strings.Index(string(p), hint) == 0
			var (uid string; active bool; hash B.Hash; exp int64)
			if ok {
				uid, active, hash, _, _, exp, ok = B.IdPubComplete(p); M.Assert(ok, 103)
				status := getStatus(true, active, exp)
				if _, b, _ := set.Search(&IdET{uid: uid, Hash: hash}); !b {
					incNbs(status, &nR, &nM, &nA, &nF)
				}
				if asked.In(status) {
					ids.SearchIns(&IdET{uid: uid, Hash: hash, Status: status})
				}
			}
			p, okL = B.IdNextPubkey(false, &ir)
		}
	}
	el := SB.IdPosUid(hintD)
	uid, hash, okL := SB.IdNextUid(false, &el)
	for okL {
		ok := BA.Prefix(hintD, BA.ToDown(uid))
		if ok {
			set.SearchIns(&IdET{uid: uid, Hash: hash})
			_, inBC := B.IdHash(hash)
			active := inBC
			if active {
				_, active, _, _, _, _, ok = B.IdUidComplete(uid); M.Assert(ok, 104)
			}
			status := getStatus(inBC, active, 0)
			if !inBC {
				incNbs(status, &nR, &nM, &nA, &nF)
			}
			if asked.In(status) {
				ids.SearchIns(&IdET{uid: uid, Hash: hash, Status: status})
			}
		}
		uid, hash, okL = SB.IdNextUid(false, &el)
	}
	if len(hint) <= B.PubkeyLen {
		el := SB.IdPosPubkey(B.Pubkey(hint))
		p, hash, okL := SB.IdNextPubkey(false, &el)
		for okL {
			ok := strings.Index(string(p), hint) == 0
			if ok {
				_, inBC := B.IdHash(hash)
				active := inBC
				if active {
					_, active, _, _, _, _, ok = B.IdPubComplete(p); M.Assert(ok, 105)
				}
				status := getStatus(inBC, active, 0)
				var uid string
				_, _, uid, _, _, ok = SB.IdHash(hash); M.Assert(ok, 106)
				if _, b, _ := set.Search(&IdET{uid: uid, Hash: hash}); !b {
					incNbs(status, &nR, &nM, &nA, &nF)
				}
				if asked.In(status) {
					ids.SearchIns(&IdET{uid: uid, Hash: hash, Status: status})
				}
			}
			p, hash, okL = SB.IdNextPubkey(false, &el)
		}
	}
	return
} //Find

func SentCerts (h SB.Hash, pubkey B.Pubkey, inBC, withTree bool) (sentNb, sentFNb int, certified *A.Tree) { //*IdET
	certified = A.New()
	sentNb = 0
	sentFNb = 0
	var posB B.CertPos
	if inBC {
		okB := B.CertFrom(pubkey, &posB)
		if okB {
			sentNb = posB.CertPosLen()
		}
		var posS SB.CertPos
		okS := SB.CertFrom(pubkey, &posS)
		if okS {
			sentFNb = posS.CertPosLen()
		}
		if withTree {
			var from, to B.Pubkey
			if okB {
				from, to, okB = posB.CertNextPos()
			}
			for okB {
				idE := new(IdET)
				uid, _, hash, _, _, _, b := B.IdPubComplete(to); M.Assert(b, 100)
				idE.uid = uid
				idE.Hash = hash
				idE.Status = Member // Status of the certification (Newcomer or Member)
				_, idE.limit, b = B.Cert(from, to); M.Assert(b, 101)
				_, b, _ = certified.SearchIns(idE); M.Assert(!b, 102)
				from, to, okB = posB.CertNextPos()
			}
			var toH B.Hash
			if okS {
				from, toH, okS = posS.CertNextPos()
			}
			for okS {
				idE := new(IdET)
				idE.Hash = toH
				var b bool
				to, _, idE.limit, b = SB.Cert(from, toH)
				if b {
					idE.uid, b = B.IdPub(to)
					if !b {
						_, _, idE.uid, _, _, b = SB.IdHash(toH)
					}
				}
				M.Assert(b, 103)
				idE.Status = Newcomer
				certified.SearchIns(idE)
				from, toH, okS = posS.CertNextPos()
			}
		}
	}
	return
} //SentCerts

func RecCerts (h SB.Hash, pubkey B.Pubkey, inBC, sandboxIncluded bool) (certifiers *A.Tree, certifiersA B.PubkeysT) { //*IdET
	certifiers = A.New()
	var posB B.CertPos
	okB := inBC
	if okB {
		okB = B.CertTo(pubkey, &posB)
	}
	recNb := 0
	if okB {
		recNb = posB.CertPosLen()
	}
	var (okS = false; toH B.Hash; posS SB.CertPos)
	recFNb := 0
	if sandboxIncluded {
		if inBC {
			_, _, toH, _, _, _, okS = B.IdPubComplete(pubkey)
			if okS {
				okS = SB.CertTo(toH, &posS)
			}
		} else {
			okS = SB.CertTo(h, &posS)
		}
		if okS {
			recFNb = posS.CertPosLen() // À corriger : Enlever les émetteurs qui ne sont plus membres
		}
	}
	certifiersA = make(B.PubkeysT, recNb + recFNb)
	i := 0
	var from, to B.Pubkey
	if okB {
		from, to, okB = posB.CertNextPos()
	}
	for okB {
		certifiersA[i] = from
		idE := new(IdET)
		uid, _, hash, _, _, _, b := B.IdPubComplete(from); M.Assert(b, 106)
		idE.uid = uid
		idE.Hash = hash
		idE.Status = Member
		_, idE.limit, b = B.Cert(from, to); M.Assert(b, 107)
		_, b, _ = certifiers.SearchIns(idE); M.Assert(!b, 108)
		i++
		from, to, okB = posB.CertNextPos()
	}
	if okS {
		from, toH, okS = posS.CertNextPos()
	}
	for okS {
		var (b bool; hash B.Hash)
		idE := new(IdET)
		idE.uid, _, hash, _, _, _, b = B.IdPubComplete(from)
		if b {
			certifiersA[i] = from // À corriger : vérifier que from est toujours membre et sinon ajouter une croix à côté du rond
			i++
			idE.Hash = hash
			idE.Status = Newcomer
			_, _, idE.limit, b = SB.Cert(from, toH); M.Assert(b, 110)
			certifiers.SearchIns(idE)
		}
		from, toH, okS = posS.CertNextPos()
	}
	return
} //RecCerts

func Get (hash SB.Hash) (uid string, pubkey B.Pubkey, block, application, pendingApplication int32, limitDate int64, inBC, member, ok bool) {
	pubkey, inBC = B.IdHash(hash)
	ok = false
	if inBC {
		uid, member, _, block, application, limitDate, ok = B.IdPubComplete(pubkey); M.Assert(ok, 100);
		if _, _, _, bnb, _, ok := SB.IdHash(hash); ok {
			//Assertion not always verified (bnb: blockstamp for membership application in sandbox; application: block of last membership application in BC)
			//M.Assert(bnb >= application, uid, application, bnb, 100)
			pendingApplication = bnb
		} else {
			pendingApplication = -1
		}
	} else if _, pubkey, uid, pendingApplication, limitDate, ok = SB.IdHash(hash); ok {
		member = false
		block = - 1
		application = -1
	}
	return
} //Get

func certifiersOf (p B.Pubkey, member bool, certifiers B.PubkeysT) B.PubkeysT {
	n := 0
	if certifiers != nil {
		n = len(certifiers)
	}
	i := 0
	if member {
		n++
		i = 1
	}
	certs := make(B.PubkeysT, n)
	if member {
		certs[0] = p
	}
	if certifiers != nil {
		for _, c := range certifiers {
			certs[i] = c
			i++
		}
	}
	return certs
} //certifiersOf

func NotTooFar (p B.Pubkey, member bool, certifiers B.PubkeysT) (num, denom int, ratio float64, ok bool) {
	num, denom, ratio = B.Distance(p, certifiersOf(p, member, certifiers))
	ok = ratio >= B.Pars().Xpercent
	return
} //NotTooFar

func FixCertNextDate (member bool, p B.Pubkey) (date int64, passed bool) {
	passed = member
	if member {
		date = 0
		var pos B.CertPos
		if B.CertFrom(p, &pos) {
			from, to, ok := pos.CertNextPos()
			for ok {
				block_number, _, b := B.Cert(from, to); M.Assert(b, 100)
				tm, _, b := B.TimeOf(block_number); M.Assert(b, "block_number =", block_number, 101)
				date = M.Max64(date, tm)
				from, to, ok = pos.CertNextPos()
			}
			date += int64(B.Pars().SigPeriod)
			passed = date <= B.Now()
		}
	} else {
		date = - 1
	}
	return
} //FixCertNextDate

func CalcQuality (p B.Pubkey, member bool, certifiers B.PubkeysT) (num, denom int, quality float64) {
	num, denom, quality = B.Quality(p,certifiersOf(p, member, certifiers))
	return
} //CalcQuality

func CalcCentrality (p B.Pubkey, inBC bool) (centrality float64) {
	if inBC {
		centrality = C.CountOne(p)
	} else {
		centrality = 0.
	}
	return
} //CalcCentrality
